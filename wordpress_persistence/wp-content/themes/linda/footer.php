<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Linda
 */

?>

<footer id="footer">
	<p class="copyright">&copy; LINDA BRASIL. All rights reserved. </br>Design: <a href="http://otus-solutions.com.br">OTUS SOLUTIONS</a>.</p>
</footer>
