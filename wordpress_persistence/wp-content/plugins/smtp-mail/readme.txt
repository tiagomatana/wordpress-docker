=== SMTP Mail ===
Contributors: photoboxone
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Donate link: http://photoboxone.com/donate/?for=smtp-mail
Tags: smtp setting, mail, data, smtp simple, sendmail, send mail, send test, mailer, mail function, php mailer 
Requires at least: 3.5 
Tested up to: 5.1
Stable tag: 1.1.5

Setting SMTP, mail function to send mail, save data in WP Mail ( php mailer ).
It is extremely easy to configure and very fast. 

== Description == 

SMTP Mail is a plugin config or setting SMTP or mail function to send mail and save data in WP Mail ( php mailer ).

It is extremely easy to configure and very fast.

= The plugin allows using : =

* Config SMTP
* Setting SMTP 
* Mail function
* Send test in wp-admin
* Mail data 

[How to configure an SMTP Mail plugin](http://photoboxone.com/how-to-configure-an-smtp-mail-plugin/)?

== Installation ==
#### Thank you for your interest in the SMTP Mail plugin.
### Minimum requirements.

*   Wordpress 3.0+
*   PHP 5.x 
*   MySQL 5.x 

This section describes how to install the [plugin wordpress](http://photoboxone.com/how-to-install-plugin-in-wordpress/) and get it working. 
e.g. 

1. Upload folder 'plugin-name' to the `/wp-content/plugins/` directory.
2. Activate the "SMTP Mail" plugin through the '[Plugins](http://photoboxone.com/category/plugins/)' menu in the WordPress. 
3. Go to "SMTP Mail" in Setting.

If any problem occurs, please read [documents Wordpress](http://photoboxone.com/category/documents/).

== Screenshots ==

1. SMTP Mail - Setting.
2. SMTP Mail - Send test.
3. SMTP Mail - Mail data - list.
4. SMTP Mail - Mail data - detail.

== Changelog == 

= 1.1.5 =
* Add recommend plugins.

= 1.1.4 =
* Bug fixes. ( Save cache ).

= 1.1.3 =
* Tested for Wordpress 5.1
* Bug fixes.

= 1.1.2 =
* Tested for Wordpress 5.0.3
* Add setting [detail form] Tab data.

= 1.1.1 =
* Tested for Wordpress 5.0.2

= 1.1.0 =
* Update final version.

= 1.0.4.3 =
* Update data.

= 1.0.4.2 =
* Show list data submit.
* Add setting [save data].

= 1.0.4.1 =
* Fix set name.

= 1.0.4 =
* Add feature: save data submit when send mail.
* Tested for Wordpress 4.9.8.

= 1.0.3.9 =
* Tested for Wordpress 4.9.7.

= 1.0.3.8 =
* Update testing script.
* Tested for Wordpress 4.9.6.

= 1.0.3.7 =
* Update Setting Layout.

= 1.0.3.6 =
* Update Setting.

= 1.0.3.5 =
* Tested for Wordpress 4.9.5.

= 1.0.3.4 =
* Update core.

= 1.0.3.3 =
* Update Setting.

= 1.0.3.2 =
* Update core.

= 1.0.3.1 =
* Update for Wordpress 4.9.4.

= 1.0.3 =
* Update core.

= 1.0.2 =
* Update setting and stylesheet.

= 1.0.1 =
* Update for Wordpress 4.8.2.

= 1.0.0 =
* The first version.